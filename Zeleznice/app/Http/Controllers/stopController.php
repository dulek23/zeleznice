<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Stop;

class stopController extends Controller
{

        public function search(Request $request){
            if($request->ajax()){
                $output= "";
                $stops= DB::table('stops')->where('stop_name','LIKE','%'.$request->search."%")->orderBy('stop_name')->get();

                if($stops){
                    foreach ($stops as $s){
                        $output.='<tr>'.
                            '<td><a style="color:black; font-size:15px; ">' .$s->stop_name.'</a></td>'.
                            '</tr>';
                    }
                    return response($output);
                }
            }

            return view('pocetna');
        }

        public function postData(Request $request){
            $pol=$request->input('search_polazak');
            $dol=$request->input('search_dolazak');
            $dat=$request->input('datum');


            if(!empty($pol) && !empty($dol)) {
                $pol_id = DB::table('stops')
                    ->select('id')
                    ->where('stop_name', '=', $pol)
                    ->get();

                $dol_id = DB::table('stops')
                    ->select('id')
                    ->where('stop_name', '=', $dol)
                    ->get();

                $p_id = $pol_id[0]->id;
                $d_id = $dol_id[0]->id;
                
                if ($p_id != $d_id) {


                if ($p_id < $d_id) {
                    if ($p_id > 0 && $d_id < 11) {
                        $linija = 1;
                    } else if ($p_id > 9 && $d_id < 22) {
                        $linija = 3;
                    } else if (($p_id == 10 || $p_id > 21) && $d_id < 30) {
                        $linija = 5;
                    } else {
                        $linija = NULL;
                    }

                    $raspored = DB::table('trips')
                        ->select('id', 'broj', 'arrival', 'route_id', DB::raw('SUM(travel_time) AS minuti'))
                        ->whereBetween('stop_id', [$p_id, $d_id - 1])
                        ->where('route_id', '=', $linija)
                        ->groupBy('broj')
                        ->get();

                } else {
                    if ($d_id > 0 && $p_id < 11) {
                        $linija = 2;
                    } else if ($d_id > 9 && $p_id < 22) {
                        $linija = 4;
                    } else if (($d_id == 10 || $d_id > 21) && $p_id < 30) {
                        $linija = 6;
                    } else {
                        $linija = NULL;
                    }

                    $raspored = DB::table('trips')
                        ->select('broj', 'arrival', 'route_id', DB::raw('SUM(travel_time) AS minuti'))
                        ->whereBetween('stop_id', [$d_id + 1, $p_id])
                        ->where('route_id', '=', $linija)
                        ->groupBy('broj')
                        ->get();
                }

                if (!empty($linija)) {
                    return view('pocetna', compact('pol', 'dol', 'dat', 'raspored'));
                } else {
                    $msg_error = "Ne postoje vozovi za datu liniju!";
                    return view('pocetna', compact('msg_error'));
                }

            }else{
                    $msg_error = "Ne postoje vozovi za datu liniju!";
                    return view('pocetna', compact('msg_error'));
            }
            }else{
                $msg="Morate izabrati obe stanice!";
                return view('pocetna', compact('msg'));
            }

        }
}
