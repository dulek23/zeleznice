<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Red voznje - Srbija voz</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/post.js') }}" type="text/javascript"></script>
</head>
<body>
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-4">
                <a href="{{URL::to('/')}}"><img src="/img/logo.png" class="img-fluid"></a>
            </div>

        </div>

        <div class="row pt-5">
            <div class="col-md-4 col-sm-12 pl-5" style="background: #888abd;">
                <div class="pt-2 pb-2"><span style="font: 29px bold; color: white">RED VOŽNJE</span></div>
                <div><span style="font-size: 18px; color: white"><?php echo date("d-m-Y"); ?></span></div>
                <div class="pt-2 pb-2"><span style="font: 18px bold; color: white">DIREKTNI VOZOVI</span></div>

                <div>
                    <form method="post" action="{{ route('postData') }}">
                        {{ csrf_field() }}
                    <div class="form-group">
                        <div>
                            <label for="">Polazak</label>
                        </div>

                        <div>
                            <input type="text" style="width: 200px;" class="form-controll" id="search_polazak" name="search_polazak" autocomplete="off">
                            <div id="stop_polazak" style="background-color: white; width: 200px; font-weight: 400;"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <label for="">Dolazak</label>
                        </div>

                        <div>
                            <input type="text" style="width: 200px;" class="form-controll" id="search_dolazak" name="search_dolazak" autocomplete="off">
                            <div id="stop_dolazak" style="background-color: white; width: 200px; font-weight: 400;"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <label for="">Datum</label>
                        </div>

                        <div>
                            <input style="width: 200px;" type="date" name="datum" data-date-format="DD MM YYYY" value="<?php echo date("Y-m-d"); ?>" min="<?php echo date("Y-m-d"); ?>">
                        </div>

                        <div class="pt-4">
                            <button type="submit" name="trazi" id="trazi" class="btn btn-dark">Trazi</button>
                        </div>
                    </div>

                    </form>
                </div>
            </div>

            <div class="col-md-8 col-sm-12">

                @empty($msg)
                    @empty($msg_error)
                    @isset($raspored)
                <div class="pb-3">
                    <span style="font-size: 20px; font-weight: 700; color: #4d4df9;">
                        Lista vozova od stanice: <span style="color: #cc0b0b; font-size: 25px;">{{ $pol ?? '' }}</span> do stanice: <span style="color: #cc0b0b; font-size: 25px;">{{ $dol ?? '' }}</span>
                    </span>
                </div>

                <div class="table-responsive">
                    <table id="red_voznje" class="table table-sm table-bordered">
                            <tr style="background-color: #5298e4">
                                <th>Broj linije</th>
                                <th>Polazak</th>
                                <th>Datum</th>
                                <th>Dolazak</th>
                                <th>Putuje</th>
                            </tr>

                            @foreach($raspored as $r)
                            <tr>
                                <?php $ti=strtotime($r->arrival); ?>
                                <td>{{ $r->route_id }}</td>
                                <td>{{ $r->arrival }}</td>
                                <td>{{ $dat }}</td>
                                <td><?php echo $t=date("H:i", $ti+($r->minuti*60)); ?></td>
                                <td>
                                    <?php
                                    if(($r->minuti)>60){
                                        $h=intdiv($r->minuti, 60);
                                        $m=($r->minuti) %60;
                                        echo $h."h ".$m."min";
                                    }else{
                                        echo $r->minuti." min";
                                    }
                                    ?>
                                </td>
                            </tr>
                            @endforeach

                    </table>
                </div>
                    @endisset
                    @endempty
                    @isset($msg_error)
                        <div class="pl-5">
                            <h3><?php echo $msg_error; ?></h3>
                        </div>
                        @endisset
                    @empty($raspored)
                        <?php echo ""; ?>
                    @endempty
                @endempty

                @isset($msg)
                     <div class="pl-5">
                         <h3><?php echo $msg; ?></h3>
                     </div>
                @endisset

            </div>
        </div>

        <div class="row pt-5 pb-5" style="background-color: #5298e4">
            <div class="col-md-12 d-flex justify-content-center">
                <div>
                    <div><span style="color: white;">Акционарско друштво за железнички превоз путника „Србија Воз“, Београд</span></div>
                    <div class="pt-2"><span style="color: white;">Немањина 6, 11000 Београд</span></div>
                    <div class="pt-2"><span style="color: white;">ПИБ: 109108438</span></div>
                    <div class="pt-2"><span style="color: white;">+381 11 361 6722</span></div>
                </div>
            </div>

        </div>
    </div>

        <script type="text/javascript">
            $('#search_polazak').on('keyup', function () {
                var value_pol = $(this).val();

                if(value_pol!=''){

                    $.ajax({
                        type: 'get',
                        url: "{{ URL::to('/') }}",
                        data:{
                            search: value_pol,
                        } ,

                        success: function (data) {
                            //$('#initial_table').hide();
                            $('#stop_polazak').fadeIn();
                            $('#stop_polazak').html(data);
                        },

                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("AJAX error: " + textStatus + ':' + errorThrown);
                        },

                    });

                }

                $('#stop_polazak').on('click','td', function () {
                    $('#search_polazak').val($(this).text());
                    $('#stop_polazak').fadeOut();
                });

            });



            $('#search_dolazak').on('keyup', function () {
                var value_dol = $(this).val();

                if(value_dol!=''){

                    $.ajax({
                        type: 'get',
                        url: "{{ URL::to('/') }}",
                        data:{
                            search: value_dol,
                        } ,

                        success: function (data) {
                            //$('#initial_table').hide();
                            $('#stop_dolazak').fadeIn();
                            $('#stop_dolazak').html(data);
                        },

                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("AJAX error: " + textStatus + ':' + errorThrown);
                        },

                    });

                }

                $('#stop_dolazak').on('click','td', function () {
                    $('#search_dolazak').val($(this).text());
                    $('#stop_dolazak').fadeOut();
                });

            });

        </script>

</body>
</html>
