###########################################################################################
  1.    	Name
###########################################################################################
   
	Timetable for trains

###########################################################################################
  2.		Description
###########################################################################################

	This project was made for railways of Serbia. This website should allow people to search the timetable.
	There are two pull-down menus from which you can select a starting station and an arrival station.
	Also, there is a field from which you can select a date of your trip. Only if both stations are selected, it will show scheldue.
	The timetable will show departure time, arrival time and travel time, so passengers will have detailed information for the timetable.

###########################################################################################
  3.	Programming languages
###########################################################################################
	
	The following programming languages are used to realize the project:

	Laravel framework
	PHP
	MYSQL

	HTML
	Bootstrap